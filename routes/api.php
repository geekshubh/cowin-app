<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::prefix('auth')->group(function () {
    Route::post('register', 'AuthController@register');
    Route::post('login', 'AuthController@login');
    Route::get('refresh', 'AuthController@refresh');

    Route::group(['middleware' => 'auth:api'], function(){
        Route::get('user', 'AuthController@user');
        Route::post('logout', 'AuthController@logout');
    });
});
//Route::get('/cowin',['uses' => 'UserController@cowin', 'as'=>'cowin']);
//Route::get('/date',['uses' => 'UserController@date', 'as' => 'date']);
Route::group(['middleware' => 'auth:api'], function(){
    // Users
    Route::get('users', 'UserController@index');
    Route::get('users/{id}', 'UserController@show');
    Route::get('roles',['uses'=>'RoleController@index','as'=>'roles.index']);
    Route::post('roles/store',['uses'=>'RoleController@store','as'=>'roles.store']);
    Route::get('roles/{id}/edit',['uses'=>'RoleController@edit','as'=>'roles.edit']);
    Route::put('roles/{id}/update',['uses'=>'RoleController@update','as'=>'roles.update']);
    Route::post('/users/verify-email',['uses' => 'AuthController@verify_email', 'as'=>'users.verify_email']);
    Route::post('/users/save-pincode',['uses' => 'UserController@save_pincode', 'as'=>'users.save_pincode']);
});
