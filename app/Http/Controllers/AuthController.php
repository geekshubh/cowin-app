<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Jobs\verifyEmailOtpJob;

class AuthController extends Controller
{

    public function register(Request $request)
    {
        $v = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password'  => 'required|min:3',
            'age' => 'required|digits:2',
        ]);

        if ($v->fails())
        {
            return response()->json([
                'status' => 'error',
                'errors' => $v->errors()
            ], 422);
        }

        $email_otp_generate = rand(1000,9999);
        $mobile_otp_generate = rand(1000,9999);
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->email_otp = $email_otp_generate;
        $user->password = bcrypt($request->password);
        $user->phone = $request->phone;
        $user->phone_otp = $mobile_otp_generate;
        $user->age = $request->age;
        $user->role_id = $request->role_id;
        $user->save();

        verifyEmailOtpJob::dispatch($user);
        return response()->json(['status' => 'success','user' => $user], 200);
    }

    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if ($token = $this->guard()->attempt($credentials)) {
            return response()->json(['status' => 'success','token' => $token], 200)->header('Authorization', $token);
        }

        return response()->json(['error' => 'login_error'], 401);
    }

    public function logout()
    {
        $this->guard()->logout();

        return response()->json([
            'status' => 'success',
            'msg' => 'Logged out Successfully.'
        ], 200);
    }

    public function user(Request $request)
    {
        $user = User::find(Auth::user()->id);

        return response()->json([
            'status' => 'success',
            'data' => $user
        ]);
    }

    public function refresh()
    {
        if ($token = $this->guard()->refresh()) {
            return response()
                ->json(['status' => 'success'], 200)
                ->header('Authorization', $token);
        }

        return response()->json(['error' => 'refresh_token_error'], 401);
    }

    private function guard()
    {
        return Auth::guard();
    }

    public function verify_email(Request $request)
    {
        if(Auth::user()) {
            $user = Auth::user();
            if($user->email_otp == $request->email_otp) {
                $user->email_verified_at = Carbon::now('Asia/Kolkata');
                $user->update();
                return response()->json(['status' => 'success', 'message' => 'OTP Verified!']);
            } else {
                return response()->json(['message' => 'Wrong OTP!', 'status' => 'error']);
            }
        } else {
            return response()->json(['message' => 'Unauthorized', 'status' => 'error']);
        }
    }
}
