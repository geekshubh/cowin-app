<?php

namespace App\Http\Controllers;

use App\Jobs\SendSlotEmailJob;
use App\User;
use App\UserDetail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function index()
    {
        $users = User::all();
        return $users;
    }

    public function show(Request $request, $id)
    {
        $user = User::find($id);
        return $user;
    }

    public function save_pincode(Request $request)
    {
        if(Auth::user()) {
            foreach($request->pincodes as $pincode) {
                $user_details = new UserDetail;
                $user_details->user_id = Auth::id();
                $user_details->pincode = $pincode;
                $user_details->save();
            }
            return response()->json(['status' => 'success', 'message' => 'Data Saved Successfully']);
        } else {
            return response()->json(['status' => 'error', 'message' => 'Not Logged In']);
        }

    }

    public function cowin()
    {
        $pincodes = UserDetail::select('pincode')->distinct()->get();
        $array = [];
        $current_date = Carbon::now('Asia/Kolkata')->format('d-m-Y');
        foreach($pincodes as $pincode) {
            $jsonurl = "https://cdn-api.co-vin.in/api/v2/appointment/sessions/public/calendarByPin?pincode=".$pincode['pincode']."&date=".$current_date;
            $json = file_get_contents($jsonurl);
            $decode = json_decode($json);
            if($decode->centers == []) {
                //$counter = $counter + 1;
            } else {
                foreach ($decode->centers as $center)
                {
                    for($i = 0; $i < count($center->sessions); $i++)
                    {
                        if($center->sessions[$i]->available_capacity != 0 && $center->sessions[$i]->date == $current_date)
                        {
                            array_push($array, $center);
                        }
                    }
                }
            }

        }
        for($i = 0; $i < count($array); $i++) {
            $pincode = $array[$i]->pincode;
            foreach($array[$i]->sessions as $session) {
                if($session->min_age_limit == 18 && $session->date == $current_date) {
                    $users = User::where('age', '>=', '18')->where('age', '<', '45')->get();
                    foreach($users as $user) {
                        SendSlotEmailJob::dispatch($user);
                    }
                } elseif ($session->min_age_limit == 45 && $session->date == $current_date) {
                    $users = User::where('age', '>=', '45')->get();
                    foreach($users as $user) {
                        SendSlotEmailJob::dispatch($user);
                    }
                }

            }
        }
    }

//    public function date()
//    {
//        $date = Carbon::now('Asia/Kolkata')->format('d-m-Y');
//        return $date;
//    }

}
