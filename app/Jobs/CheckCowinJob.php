<?php

namespace App\Jobs;

use App\User;
use App\UserDetail;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CheckCowinJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $pincodes = UserDetail::select('pincode')->distinct()->get();
        $array = [];
        $current_date = Carbon::now('Asia/Kolkata')->format('d-m-Y');
        foreach($pincodes as $pincode) {
            $jsonurl = "https://cdn-api.co-vin.in/api/v2/appointment/sessions/public/calendarByPin?pincode=".$pincode['pincode']."&date=".$current_date;
            $json = file_get_contents($jsonurl);
            $decode = json_decode($json);
            if($decode->centers == []) {
                //$counter = $counter + 1;
            } else {
                foreach ($decode->centers as $center)
                {
                    for($i = 0; $i < count($center->sessions); $i++)
                    {
                        if($center->sessions[$i]->available_capacity != 0 && $center->sessions[$i]->date == $current_date)
                        {
                            array_push($array, $center);
                        }
                    }
                }
            }

        }
        for($i = 0; $i < count($array); $i++) {
            $pincode = $array[$i]->pincode;
            foreach($array[$i]->sessions as $session) {
                if($session->min_age_limit == 18 && $session->date == $current_date) {
                    $users = User::where('age', '>=', '18')->where('age', '<', '45')->get();
                    foreach($users as $user) {
                        SendSlotEmailJob::dispatch($user);
                    }
                } elseif ($session->min_age_limit == 45 && $session->date == $current_date) {
                    $users = User::where('age', '>=', '45')->get();
                    foreach($users as $user) {
                        SendSlotEmailJob::dispatch($user);
                    }
                }

            }
        }
    }
}
